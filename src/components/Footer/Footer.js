import React from 'react';
import { BrowserRouter as Link } from "react-router-dom";

export default function Footer() {

  return (
    <React.Fragment>
      <div className="footer container">
        <div className="footer__col">
          <h3 className="h3">Menu</h3>
          <ul className="footer__menu">
            <li><Link to={`/`}>Accueil</Link></li>
            <li><Link to={`/portfolio`}>Portfolio</Link></li>
            <li><Link to={`/blog`}>Le blog</Link></li>
            <li><Link to={`/contact`}>Contact</Link></li>
          </ul>
        </div>
        <div className="footer__col">
          <h3 className="h3">Gardons le contact</h3>
          <p className="footer__social">
            <a href="http://www.facebook.com" target="_blank" className="icon-facebook" rel="noopener noreferrer"></a>
            <a href="http://www.facebook.com" target="_blank" className="icon-twitter" rel="noopener noreferrer"></a>
            <a href="http://www.facebook.com" target="_blank" className="icon-linkedin" rel="noopener noreferrer"></a>
            <a href="http://www.facebook.com" target="_blank" className="icon-instagram" rel="noopener noreferrer"></a>
            <a href="http://www.facebook.com" target="_blank" className="icon-pinterest" rel="noopener noreferrer"></a>
          </p>
        </div>
      </div>
      <div className="mentions container">
        <p className="mentions__copyright">&copy;2020 | <Link to={`/`}>Yannick Ratel - Intégrateur XHTML/CSS, WordPress</Link></p>
        <p className="mentions__baseline"><span><Link to={`/menntions-legales`}>Mentions légales</Link></span><span><Link to={`/contact`}>Contact</Link></span></p>
      </div>
    </React.Fragment>
  )

}
