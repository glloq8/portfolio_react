import React from 'react';
import { NavLink } from "react-router-dom";

export default function NavBar() {
    return (
        <React.Fragment>
            <div className="navbar">
                <ul className="menu container">
                    <li><NavLink exact to={`/`}>Accueil</NavLink></li>
                    <li><NavLink exact to={`/portfolio`}>Portfolio</NavLink></li>
                    <li><NavLink to={`/blog`}>Le blog</NavLink></li>
                    <li><NavLink to={`/contact`}>Contact</NavLink></li>
                </ul>
            </div>
        </React.Fragment>
    )
}
