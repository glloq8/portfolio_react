import React from 'react';
import { Link } from "react-router-dom";

import Domaine from './Domaine.js';

import responsive from './responsive-devices_icons.svg';
import cart from './shopping-cart.svg';
import email from './email.svg';
import maintenance from './maintenance.svg';

const titreDomaines = 'Domaines d\'actions';
const listeDomaines = [
  {
    id: 1,
    icon: responsive,
    title: 'Site internet',
    description: 'Vous souhaitez créer un site internet pour votre entreprise ou association, donner un coup de fraîcheur à votre site internet existant, ou améliorer votre présence en ligne ? N\'hésitez pas à me parler de votre projet.'
  },
  {
    id: 2,
    icon: cart,
    title: 'Site e-commerce',
    description: 'Vous désirez vendre vos produits en ligne ? Nous pouvons trouver ensemble la solution la plus adaptée à vos besoins.'
  },
  {
    id: 3,
    icon: email,
    title: 'Newsletter',
    description: 'Vous souhaitez communiquer avec vos clients ou partenaires, je peux vous aider à réaliser votre communication par email.'
  },
  {
    id: 4,
    icon: maintenance,
    title: 'Maintenance',
    description: 'Je peux vous accompagner dans la maintenance de votre site. Mises à jour, petites opérations de maintenance, bugs,...'
  },
];

export default function Domaines(props) {
  return (
    <React.Fragment>
      <div className="domaines container">
        <h3 className="h3">{titreDomaines}</h3>
        <ul className="domaines__liste">
          {listeDomaines.map((domaine) => (
              <Domaine key={domaine.id} data={domaine} />
          ))}
        </ul>
      </div>
      <div className="contact-home container">
        <p>Pour tout renseignement ou toute question concernant votre site internet ou votre projet, n'hésitez pas à</p>
        <p><Link to={`/contact`} className="bt-primary">me contacter</Link></p>
      </div>
    </React.Fragment>
  );
}
