import React from 'react';

export default function Domaine(props) {
  const { data: domaine } = props;

  return (
    <React.Fragment>
      <li>
        <h4>{domaine.title}</h4>
        <p className="domaine__icon"><img src={domaine.icon} alt={domaine.title} /></p>
        <div className="domaine__desc">{domaine.description}</div>
      </li>
    </React.Fragment>
  )
}
