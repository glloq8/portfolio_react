import React from 'react';
import Post from './Post.js';

import photo from './programming-1009134_1280.jpg';

const titreBlog = "Du côté du blog";

const listePosts = [
  {
    id: 1,
    slug: 'article',
    photo: photo,
    title: `Librairie de codes HTML/CSS gratuits`,
    desc: `J’ai découvert, ce matin, grâce à Jonathan Menet le site FreeFrontend. Sur ce site, vous aurez accès gratuitement à une tonne d’exemples HTML/CSS pour tout type de choses. Des formulaires,...`
  },
  {
    id: 2,
    slug: 'article',
    photo: '',
    title: `Librairie de codes HTML/CSS gratuits`,
    desc: `J’ai découvert, ce matin, grâce à Jonathan Menet le site FreeFrontend. Sur ce site, vous aurez accès gratuitement à une tonne d’exemples HTML/CSS pour tout type de choses. Des formulaires,...`
  },
  {
    id: 3,
    slug: 'article',
    photo: photo,
    title: `Librairie de codes HTML/CSS gratuits`,
    desc: `J’ai découvert, ce matin, grâce à Jonathan Menet le site FreeFrontend. Sur ce site, vous aurez accès gratuitement à une tonne d’exemples HTML/CSS pour tout type de choses. Des formulaires,...`
  },
  {
    id: 4,
    slug: 'article',
    photo: photo,
    title: `Librairie de codes HTML/CSS gratuits`,
    desc: `J’ai découvert, ce matin, grâce à Jonathan Menet le site FreeFrontend. Sur ce site, vous aurez accès gratuitement à une tonne d’exemples HTML/CSS pour tout type de choses. Des formulaires,...`
  },
  {
    id: 5,
    slug: 'article',
    photo: photo,
    title: `Librairie de codes HTML/CSS gratuits`,
    desc: `J’ai découvert, ce matin, grâce à Jonathan Menet le site FreeFrontend. Sur ce site, vous aurez accès gratuitement à une tonne d’exemples HTML/CSS pour tout type de choses. Des formulaires,...`
  }
];

export default function Articles() {
  return (
    <React.Fragment>
      <div className="homeposts container">
        <h3 className="h3">{titreBlog}</h3>
        <ul className="liste-posts">
          {listePosts.map((post) => (
              <Post key={post.id} data={post} />
          ))}
        </ul>
      </div>
    </React.Fragment>
  )
}
