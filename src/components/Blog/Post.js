import React from 'react';
import { Link } from "react-router-dom";

export default function Post(props) {
  const { data: post } = props;

  return (
    <React.Fragment>
      <li>
        <Link to={`/blog/${post.id}-${post.slug}`}>
          <p className={`liste-posts__photo${post.photo ? '' : ' no-thumb'}`}>{post.photo ? <img src={post.photo} alt={post.title} /> : '' }</p>
          <h2 className="h2">{post.title}</h2>
          <div className="liste-posts__desc">{post.desc}</div>
          <p className="readmore"><span>Lire la suite</span></p>
        </Link>
      </li>
    </React.Fragment>
  )
}
