import React from 'react';
import Posts from './Posts';

export default function Blog() {
  return (
    <React.Fragment>
      <div className="blog container">
        <h3 className="h3">Blog</h3>
        <Posts />
      </div>
    </React.Fragment>
  )
}
