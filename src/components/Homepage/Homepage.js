import React from 'react';

import Domaines from '../Domaines/Domaines';
import Projets from '../Projets/Projets';
import Articles from '../Blog/Posts';

export default function Homepage() {
  return (
    <React.Fragment>
      <Domaines />
      <Projets />
      <Articles />
    </React.Fragment>
  )
}
