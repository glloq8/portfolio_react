import React from 'react';
import Projets from './Projets';

export default function Portfolio() {
  return (
    <React.Fragment>
      <div className="portfolio container">
        <h3 className="h3">Portfolio</h3>
        <Projets />
      </div>
    </React.Fragment>
  )
}
