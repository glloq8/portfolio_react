import React from 'react';
import { Link } from "react-router-dom";

export default function Projet(props) {
  const { data: projet } = props;

  return (
    <React.Fragment>
      <li>
        <Link to={`/portfolio/${projet.id}-${projet.slug}`}>
          <p className="liste-projets__photo"><img src={projet.icon} alt={projet.title} /></p>
          <h2>{projet.title}</h2>
          <p className={`support ${projet.support.toLowerCase()}`}>{projet.support}</p>
        </Link>
      </li>
    </React.Fragment>
  )
}
