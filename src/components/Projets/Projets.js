import React from 'react';
import Projet from './Projet.js';

import carrelage from './carrelage.jpg';
import ouvert from './ouvert.png';
import cube from './cube.png';
import av08 from './av08.jpg';

const titreProjets = "Mes dernières réalisations";

const listeProjets = [
  {
    id: 1,
    slug: 'carrelage',
    icon: carrelage,
    title: 'Intégration e-commerce de carrelage',
    support: 'WordPress'
  },
  {
    id: 2,
    slug: 'ouvert',
    icon: ouvert,
    title: 'Intégration HTML WordPress du site Ouvert',
    support: 'WordPress'
  },
  {
    id: 3,
    slug: 'cube',
    icon: cube,
    title: 'Intégration du site du jeu intéractif Cube your Life',
    support: 'Prestashop'
  },
  {
    id: 4,
    slug: 'av08',
    icon: av08,
    title: 'Intégration Prestashop e-commerce de chèches',
    support: 'Prestashop'
  },
];

export default function Projets() {
  return (
    <React.Fragment>
      <div className="featured container">
        <h3 className="h3">{titreProjets}</h3>
        <ul className="liste-projets">
          {listeProjets.map((projet) => (
              <Projet key={projet.id} data={projet} />
          ))}
        </ul>
      </div>
    </React.Fragment>
  )
}
