import React from 'react';
import logo from './logo-yannick-ratel-html-css.svg';

const headerTitle = 'Yannick Ratel';
const headerSubTitle = 'Intégrateur XHTML/CSS, WordPress';

class Toggle extends React.Component {
  handleClick = () => {
    console.log('this vaut :', this.value);
  }

  render() {
    return (
      <React.Fragment>
        <div state="close" className="toggle-menu {this.state.isToggleOn ? '' : 'open'}">
          <button onClick={this.handleClick}></button>
          <div className="hamburger">
              <span></span>
              <span></span>
              <span></span>
          </div>
          <div className="cross">
              <span></span>
              <span></span>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default function Header() {
    return (
        <React.Fragment>
            <div className="header">
              <Toggle />
              <p className="header__logo"><img src={logo} alt="Logo" /></p>
              <h1 className="header__title">{headerTitle}</h1>
              <p className="header__subtitle">{headerSubTitle}</p>
            </div>
        </React.Fragment>
    )
}
