import React from 'react';

export default function Contact() {
  return (
    <React.Fragment>
      <div className="contact container">
        <h3 className="h3">Contact</h3>
        <div className="content-page">
          <p>Vous souhaitez me contacter pour votre site, vous voulez des informations sur la création d’un site web ? Remplissez le formulaire ci-dessous :</p>
          <section className="form">
            <form action="#">
              <p>
                <label htmlFor="">Vos nom et prénom</label>
                <input type="text" />
              </p>
              <p>
                <label htmlFor="">Votre email (obligatoire)</label>
                <input type="email" />
              </p>
              <p>
                <label htmlFor="">Votre numéro de téléphone</label>
                <input type="text" />
              </p>
              <p>
                <label htmlFor="">Sujet</label>
                <select name="sujet" id="sujet">
                  <option value="Création de site">Création de site</option>
                  <option value="Création de site">Création ou intégration d'e-mailing</option>
                  <option value="Création de site">Refonte de site</option>
                  <option value="Création de site">Autre question</option>
                </select>
              </p>
              <p>
                <label htmlFor="">Votre message</label>
                <textarea name="" id="" cols="30" rows="10"></textarea>
              </p>
              <p>
                <input type="submit" className="bt-secondary" />
              </p>
            </form>
          </section>
          <p>Les informations envoyées via ce formulaire de contact ne sont pas conservées. Elles sont recueillies dans l’unique but de vous recontacter suite à votre message. Il n’en sera fait aucun usage commercial.</p>
        </div>
      </div>
    </React.Fragment>
  )
}
