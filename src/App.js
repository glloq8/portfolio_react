import React from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

import './sass/app.scss';
import Header from './components/Header/Header';
import NavBar from './components/NavBar/NavBar';
import Footer from './components/Footer/Footer';

import Homepage from './components/Homepage/Homepage';
import Portfolio from './components/Projets/Portfolio';
import SingleProjet from './components/Projets/SingleProjet';
import Blog from './components/Blog/Blog';
import Contact from './components/Contact/Contact';

function App() {
  return (
    <Router>
      <div className="App">
        <Header />
        <NavBar />
        <div className="main">
          <Route exact path="/" component={Homepage} />

          <Route exact path="/portfolio" component={Portfolio} />
          <Route exact path="/portfolio/1-carrelage" component={SingleProjet} />

          <Route path="/contact" component={Contact} />

          <Route path="/blog" component={Blog} />
        </div>
        <Footer />
      </div>
    </Router>
  );
}

export default App;
